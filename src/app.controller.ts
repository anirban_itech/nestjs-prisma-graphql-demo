import { Controller, Get, Request } from '@nestjs/common';
import { AppService } from './app.service';
import { UserService } from './user.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private userService: UserService,
  ) {}

  @Get('hello')
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('hello2')
  getHello2(@Request() req: any): string {
    console.log(req.headers);
    return this.appService.getHello();
  }

  @Get('users')
  getUser(): any {
    return this.userService.users({});
  }
}
